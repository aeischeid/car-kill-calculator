# Svelte + Bulma app

This is a very small project meant primarily as a testing ground for Svelte.dev and Bulma.io

# The Car-killer though...

It's not some cool demolition derby game. Just a simple calculator that could just as easily be done with an excel spreadsheet if I'm being honest. 

You would use it to do something like figure out how much use you need to avoid
via a new device/mode before the expense to purchace said new thing would be justified, or how long it would take to justify if you dodge x amound of use per year. In other words a cost benefit analysis tool, but aimed specifically at situations where you might be deciding between say a used Prius, a new Tesla, or just sticking with your reliable but rather inefficient old family minivan - Whether you should get an ebike or a bus pass maybe. That kind of thing. 

